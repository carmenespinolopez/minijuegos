# Minijuegos  App

Aplicación de minijuegos construida con React.js que dispone de los juegos listados a continuación:


## Tres en raya

Construido siguiendo este tutorial: https://www.youtube.com/watch?v=Yq7yWlmzsvM
Repositorio: https://github.com/accesibleprogramacion/ta-te-ti



-------------------------------

# Mini-games App

Mini-games application built with React.js that includes the games listed below:


## Tic-tac-toe

Built following this tutorial: https://www.youtube.com/watch?v=Yq7yWlmzsvM
Repository: https://github.com/accesibleprogramacion/ta-te-ti
